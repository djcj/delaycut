QT         += core gui

greaterThan(QT_MAJOR_VERSION, 4) {
  QT += widgets
  DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x000000
}

CONFIG     += qt console
TARGET      = delaycut
TEMPLATE    = app

INCLUDEPATH += src

SOURCES    += src/main.cpp\
              src/delaycut.cpp \
              src/delayac3.cpp \
              src/dragdroplineedit.cpp

HEADERS    += src/delaycut.h \
              src/sil48.h \
              src/delayac3.h \
              src/dc_types.h \
              src/dragdroplineedit.h

FORMS      += src/delaycut.ui

win32 {
  RESOURCES  += src/delaycut_ico.qrc
  RC_FILE     = src/delaycut.rc
}

!win32 {
  RESOURCES  += src/delaycut_png.qrc
  target.path = /usr/bin
  INSTALLS   += target
}

win32-g++ {
  # tested with i686-w64-mingw32.static-g++ (https://github.com/mxe/mxe)
  # QMAKE_HOST.arch doesn't seem to work
  !contains(QMAKE_TARGET.arch, x86_64):QMAKE_LFLAGS += -Wl,--large-address-aware
}

win32-msvc* {
  CONFIG += c++11 # C++11 support
  QMAKE_CXXFLAGS += /bigobj # allow big objects
  !contains(QMAKE_HOST.arch, x86_64):QMAKE_LFLAGS += /LARGEADDRESSAWARE #
  QMAKE_CFLAGS_RELEASE += -WX
  QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO += -WX


  # for Windows XP compatibility
  contains(QMAKE_HOST.arch, x86_64) {
    #message(Going for Windows XP 64bit compatibility)
    #QMAKE_LFLAGS += /SUBSYSTEM:WINDOWS,5.02 # Windows XP 64bit
  } else {
    message(Going for Windows XP 32bit compatibility)
    QMAKE_LFLAGS += /SUBSYSTEM:WINDOWS,5.01 # Windows XP 32bit
  }
}

QMAKE_CLEAN += qrc_delaycut_ico.cpp \
               qrc_delaycut_ico.o \
               qrc_delaycut_png.cpp \
               qrc_delaycut_png.o

